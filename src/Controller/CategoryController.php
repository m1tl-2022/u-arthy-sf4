<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="category")
     */
    public function index(CategoryRepository $categoryRepo): Response
    {
        $categories = $categoryRepo->findAllCategories();

        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
            'categories'=> $categories
        ]);
    }

    /**
     * @Route("/newcat", name="category.add", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function new(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash('success', 'La catégorie a bien été ajotuée.');
            return $this->redirectToRoute('category');
        }

        return $this->render('category/new.html.twig', [
            'category'=>$category,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/{id}/editcat", name="category.edit", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, Category $category): Response
    {   
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('sucess', 'La modification de la catégorie a bien été enregistrée');
            return $this->redirectToRoute('category');
        }
        
        return $this->render('category/edit.html.twig', [
            'category'=>$category,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="category.delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function delete(Request $request, Category $category): Response
    {
        if($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();

            $this->addFlash('sucess', 'La catégorie a bien été supprimée');
        }

        return $this->redirectToRoute('category');

    }
}
